NAME := image-transformer
CC = clang
LINKER = $(CC)

NASM = nasm
NASMFLAGS = -felf64 -g

BUILDDIR = build
SOLUTION_DIR = solution
BENCHDIR = bench

SRCDIR = $(SOLUTION_DIR)/src
INCDIR = $(SOLUTION_DIR)/include
OBJDIR = obj/$(SOLUTION_DIR)
BENCHSRCDIR = $(BENCHDIR)/src
OBJBENCHDIR = obj/$(BENCHDIR)

CFLAGS += $(strip $(file < $(SOLUTION_DIR)/compile_flags.txt)) -I$(INCDIR) 

all: $(OBJDIR)/bmp.o $(OBJDIR)/bmp_utils.o $(OBJDIR)/file_utils.o $(OBJDIR)/image_format.o $(OBJDIR)/sepia.o $(OBJDIR)/transformations.o $(OBJDIR)/sepia_asm.o $(OBJDIR)/main.o
	$(CC) -o $(BUILDDIR)/$(NAME) $^

build:
	mkdir -p $(BUILDDIR)
	mkdir -p $(OBJDIR)
	mkdir -p $(OBJBENCHDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(OBJDIR)/%_asm.o: $(SRCDIR)/%.asm build
	$(NASM) $(NASMFLAGS) $< -o $@


bench: $(OBJBENCHDIR)/main.o $(OBJDIR)/bmp.o $(OBJDIR)/bmp_utils.o $(OBJDIR)/file_utils.o $(OBJDIR)/image_format.o $(OBJDIR)/sepia.o $(OBJDIR)/transformations.o $(OBJDIR)/sepia_asm.o
	$(CC) -o $(BUILDDIR)/bench $^
	./$(BUILDDIR)/bench --sepia $(BENCHDIR)/resources/input.bmp
	./$(BUILDDIR)/bench --sepia-asm $(BENCHDIR)/resources/input.bmp

$(OBJBENCHDIR)/%.o: $(BENCHSRCDIR)/%.c $(OBJDIR)/%.o
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJDIR) $(BUILDDIR) $(OBJBENCHDIR)

.PHONY: all bench clean build
#include "../../solution/include/bmp.h"
#include "../../solution/include/transformations.h"
#include "../../solution/include/sepia.h"
#include "../../solution/include/file_utils.h"
#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>

const char *HELP_MESSAGE = "Usage: bench <source-image>";

void print_error(const char *msg) {
    fprintf(stderr, "%s\n", msg);
}

int main(int argc, char **argv) {
    if (argc != 1) {
        if (argc == 0) {
            print_error(HELP_MESSAGE);
            return EXIT_SUCCESS;
        }
    }
    const char *source_img_path = argv[2];
    const char *input_mode = "rb";
    FILE *input_file = NULL;
    enum open_status in_open_status = open_file(&input_file, source_img_path, input_mode);
    if (in_open_status != OPEN_OK) {
        print_error("Error while opening the file");
        return EXIT_FAILURE;
    }

    struct image img;
    enum read_status in_read_status = from_bmp(input_file, &img);
    close_file(input_file);
    if (in_read_status != READ_OK) {
        print_error("Error while reading the file");
        clear_img(&img);
        return EXIT_FAILURE;
    }

    
    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    struct image new_img;
    if (!strcmp("--rotate", argv[1])) {
        new_img = rotate(img);
    } else if (!strcmp("--sepia", argv[1])) {
        new_img = sepia_c(img);
    } else if (!strcmp("--sepia-asm", argv[1])) {
        new_img = sepia_asm(img);
    } else {
        print_error(HELP_MESSAGE);
        return EXIT_FAILURE;
    }

    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;
    long res = ((end.tv_sec - start.tv_sec) * 1000000L) +
               end.tv_usec - start.tv_usec;
    printf("Time elapsed in microseconds: %ld\n", res);

    
    clear_img(&img);
    clear_img(&new_img);
    return 0;
}

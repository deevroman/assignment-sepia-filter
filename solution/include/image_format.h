#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_FORMAT_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_FORMAT_H

#include <stdlib.h>
#include <inttypes.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_img(uint32_t width, uint32_t height);

struct image copy_img(const struct image img);

void clear_img(struct image *img);


#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_FORMAT_H

#ifndef ASSIGNMENT_SEPIA_FILTER_SEPIA_H
#define ASSIGNMENT_SEPIA_FILTER_SEPIA_H
#include <inttypes.h>
#include "image_format.h"

struct image sepia_c(struct image img);
struct image sepia_asm(struct image img);

#endif //ASSIGNMENT_SEPIA_FILTER_SEPIA_H

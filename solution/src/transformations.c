#include "transformations.h"

struct image rotate(struct image const source) {
    struct image new_img = create_img(source.height, source.width);
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            new_img.data[j * source.height + (source.height - 1 - i)] = source.data[i * (source.width) + j];
        }
    }
    return new_img;
}

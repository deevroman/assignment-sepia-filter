global sepia_sse

section .data
align 16
c1: dd 0.0, 0.168, 0.189, 0.131
align 16
c2: dd 0.0, 0.686, 0.769, 0.543
align 16
c3: dd  0.0, 0.349, 0.393, 0.272
align 16
c4: dd 255.0, 255.0, 255.0, 255.0


section .text

%macro set_xmm_register 2
  pxor %1, %1 
  pinsrb %1, byte[%2 + 0], 0 
  pinsrb %1, byte[%2 + 1], 4 
  pinsrb %1, byte[%2 + 2], 8 
  pinsrb %1, byte[%2 + 3], 12
%endmacro


%macro set_numbers 2
  set_xmm_register xmm0, %1
  lea %1, [%1 + 1]
  set_xmm_register xmm1, %1
  lea %1, [%1 + 1]
  set_xmm_register xmm2, %1

  shufps   xmm0, xmm0, %2
  shufps   xmm1, xmm1, %2
  shufps   xmm2, xmm2, %2

  cvtdq2ps xmm0, xmm0
  cvtdq2ps xmm1, xmm1
  cvtdq2ps xmm2, xmm2  
%endmacro


%macro set_coef 1
  shufps   xmm3, xmm3, %1
  shufps   xmm4, xmm4, %1
  shufps   xmm5, xmm5, %1
%endmacro


%macro step 1
  add   rcx, 4

  movdqa   xmm3, [c1]
  movdqa   xmm4, [c2]
  movdqa   xmm5, [c3]
  movdqa   xmm6, [c4]
  
  push  rdi
  %if %1 == 1
    set_coef 0xE7
    set_numbers rdi, 0xC0
    %define NXT_LABEL .nxt1
  %elif %1 == 2
    set_coef 0x79
    set_numbers rdi, 0xF0
    %define NXT_LABEL .nxt2
  %elif %1 == 3
    set_coef 0x9E
    set_numbers rdi, 0xFC
    %define NXT_LABEL .nxt3
  %endif
  pop   rdi

  mulps xmm3, xmm0
  mulps xmm4, xmm1
  mulps xmm5, xmm2

  addps xmm3, xmm4
  addps xmm3, xmm5


  pminsd   xmm3, xmm6
  cvtps2dq xmm3, xmm3

  pextrb   byte[rdx + 0], xmm3, 0 
  pextrb   byte[rdx + 1], xmm3, 4
  pextrb   byte[rdx + 2], xmm3, 8
  pextrb   byte[rdx + 3], xmm3, 12


  lea   rdi, [rdi + 3]
  lea   rdx, [rdx + 4]

  cmp   rsi, rcx 
  ja    NXT_LABEL
  ret
  NXT_LABEL:
%endmacro


; Params:
;   rdi: Source array
;   rsi: Array size
;   rdx: Destination array
sepia_sse:
  xor   rcx, rcx

  .loop:
    step 1
    step 2
    step 3
    lea   rdi, [rdi + 3]
    jmp .loop

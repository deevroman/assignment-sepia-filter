#include "bmp.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static const uint16_t BMP_SIGNATURE = 0x4D42; // little-endian => 0x424D = 'BM'
static const uint32_t RESERVED = 0;
static const uint32_t BITMAPINFOHEADER = 40;
static const uint16_t PLANES = 1;
static const uint16_t BIT_COUNT = 24;
static const uint32_t NOCOMPRESSION = 0;
static const uint32_t PIXELS_PER_METER = 0;
static const uint32_t COLOR_AMOUNT = 0;
static const uint32_t COLOR_IMPORTANT = 0;

enum header_status {
    HEADER_OK = 0,
    HEADER_ERROR,
};


enum header_status read_header(FILE *file, struct bmp_header *header) {
    if (file == NULL) {
        return HEADER_ERROR;
    }
    size_t result = fread(header, sizeof(struct bmp_header), 1, file);
    if (result != 1) {
        return HEADER_ERROR;
    } else {
        return HEADER_OK;
    }
}

bool fill_header(struct bmp_header *header, struct image *img) {
    if (header == NULL || img == NULL) {
        return false;
    }
    const uint8_t padding = calculate_padding(img->width);
    const uint32_t image_size = calculate_image_size(img, padding);
    header->bfType = BMP_SIGNATURE;
    header->bfileSize = image_size + sizeof(struct bmp_header);
    header->bfReserved = RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BITMAPINFOHEADER;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = PLANES;
    header->biBitCount = BIT_COUNT;
    header->biCompression = NOCOMPRESSION;
    header->biSizeImage = image_size;
    header->biXPelsPerMeter = PIXELS_PER_METER;
    header->biYPelsPerMeter = PIXELS_PER_METER;
    header->biClrUsed = COLOR_AMOUNT;
    header->biClrImportant = COLOR_IMPORTANT;
    return true;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    if (in == NULL) {
        return READ_FILE_NULL;
    }

    struct bmp_header header;
    enum header_status result = read_header(in, &header);
    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    } else if (result == HEADER_ERROR) {
        return READ_INVALID_HEADER;
    } else if (header.biBitCount != BIT_COUNT) {
        return READ_INVALID_BITS;
    } else if (header.biCompression != NOCOMPRESSION) {
        return READ_INVALID_COMPRESSION;
    }

    *img = create_img(header.biWidth, header.biHeight);
    if (img->data == NULL) {
        return READ_MALLOC_ERROR;
    }

    const uint8_t padding = calculate_padding(img->width);
    if (fseek(in, header.bOffBits, SEEK_SET)) {
        return READ_ERROR;
    }
    for (size_t i = 0; i < header.biHeight; i++) {
        if (fread(img->data + i * (img->width), (img->width) * sizeof(struct pixel), 1, in) != 1
            || fseek(in, padding, SEEK_CUR)) {
            clear_img(img);
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    if (out == NULL || img == NULL) {
        return WRITE_FILE_NULL;
    }
    struct bmp_header header;
    fill_header(&header, img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1
        || fseek(out, (header.bOffBits), SEEK_SET)) {
        return WRITE_HEADER_ERROR;
    }
    const uint8_t padding = calculate_padding(img->width);
    uint8_t *padding_arr = create_padding_arr(padding);
    if (padding_arr == NULL) {
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < (img->height); i++) {
        if (fwrite((img->data + img->width * i), img->width * sizeof(struct pixel), 1, out) != 1 ||
            fwrite(padding_arr, padding, 1, out) != 1) {
            free(padding_arr);
            return WRITE_ERROR;
        }
    }
    free(padding_arr);
    return WRITE_OK;
}


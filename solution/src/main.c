#include "bmp.h"
#include "file_utils.h"
#include "image_format.h"
#include "transformations.h"
#include "sepia.h"
#include <stdio.h>
#include <string.h>

const char *HELP_MESSAGE = "Usage: image-transformation <source-image> <transformed-image>";

void print(const char *msg) {
    fprintf(stdout, "%s\n", msg);
}

void print_error(const char *msg) {
    fprintf(stderr, "%s\n", msg);
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        if (argc == 0) {
            print(HELP_MESSAGE);
            return EXIT_SUCCESS;
        } else {
            print_error(HELP_MESSAGE);
            return EXIT_FAILURE;
        }
    }
    const char *source_img_path = argv[2];
    const char *transformed_img_path = argv[3];
    const char *input_mode = "rb";
    const char *output_mode = "wb";
    FILE *input_file = NULL;
    FILE *output_file = NULL;
    enum open_status in_open_status = open_file(&input_file, source_img_path, input_mode);
    if (in_open_status != OPEN_OK) {
        print_error(open_status_msg[in_open_status]);
        return EXIT_FAILURE;
    }
    enum open_status out_open_status = open_file(&output_file, transformed_img_path, output_mode);
    if (out_open_status != OPEN_OK) {
        print_error(open_status_msg[out_open_status]);
        return EXIT_FAILURE;
    }

    struct image img;
    enum read_status in_read_status = from_bmp(input_file, &img);
    if (in_read_status != READ_OK) {
        print_error(read_status_msg[in_read_status]);
        clear_img(&img);
        close_file(input_file);
        close_file(output_file);
        return EXIT_FAILURE;
    }
    struct image new_img;
    if (!strcmp("--rotate", argv[1])) {
        new_img = rotate(img);
    } else if (!strcmp("--sepia", argv[1])) {
        new_img = sepia_c(img);
    } else if (!strcmp("--sepia-asm", argv[1])) {
        new_img = sepia_asm(img);
    } else {
        print_error(HELP_MESSAGE);
        return EXIT_FAILURE;
    }
    enum write_status out_write_status = to_bmp(output_file, &new_img);
    enum close_status in_close_status = close_file(input_file);
    enum close_status out_close_status = close_file(output_file);
    clear_img(&img);
    clear_img(&new_img);

    int exit_code = EXIT_SUCCESS;
    if (out_write_status != WRITE_OK) {
        print_error(write_status_msg[out_write_status]);
        exit_code = EXIT_FAILURE;
    }
    if (in_close_status != CLOSE_OK) {
        print_error(close_status_msg[in_close_status]);
    }
    if (out_close_status != CLOSE_OK) {
        print_error(close_status_msg[out_close_status]);
    }
    return exit_code;
}

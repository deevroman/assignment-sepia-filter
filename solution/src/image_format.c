#include <string.h>
#include <printf.h>
#include "image_format.h"

struct image create_img(uint32_t width, uint32_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(width * height * sizeof(struct pixel))
    };
}

struct image copy_img(const struct image img) {
    struct image new_img = create_img(img.width, img.height);
    size_t size = sizeof(struct pixel) * img.width * img.height;
    memcpy(new_img.data, img.data, size);
    return new_img;
}

void clear_img(struct image *img) {
    if (img != NULL) {
        free(img->data);
    }
}
